package com.example.rma_project

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import com.example.rma_project.databinding.FragmentDocumentUserBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DocumentUserFragment : Fragment {

    //view binding fragment_documents_user.xml -> FragmentDocumentUserBinding
    private lateinit var binding: FragmentDocumentUserBinding

    public companion object{
        private const val TAG = "DOCUMENTS_USER_TAG"

        //receive data from activity to load documents
        public fun newInstance(categoryId: String, category: String, uid: String): DocumentUserFragment{
            val fragment = DocumentUserFragment()
            //put data to bundle intent
            val args = Bundle()
            args.putString("categoryId", categoryId)
            args.putString("category", category)
            args.putString("uid", uid)
            fragment.arguments = args
            return fragment
        }
    }

    private var categoryId = ""
    private var category = ""
    private var uid = ""

    private lateinit var pdfArrayList: ArrayList<ModelDocument>
    private lateinit var adapterDocumentUser: AdapterDocumentUser

    constructor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        if (args != null){
            categoryId = args.getString("categoryId")!!
            category = args.getString("category")!!
            uid = args.getString("uid")!!
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentDocumentUserBinding.inflate(LayoutInflater.from(context), container, false)


        //load document according to category
        Log.d(TAG, "onCreateView: Category: $category")
        if (category == "Svi dokumenti"){
            //load all documents
            loadAllDocuments()
        }
        else if (category == "Dokumenti s najviše pregleda"){
            //load most viewed documents
            loadMostViewedDocuments("viewsCount")
        }
        else{
            //load selected category documents
            loadCategorizedDocuments()
        }

        //search
        binding.searchEt.addTextChangedListener { object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    adapterDocumentUser.filter.filter(s)
                }
                catch (e: Exception){
                    Log.d(TAG, "onTextChanged: SEARCH EXCEPTION: ${e.message}")
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        } }

        return binding.root
    }

    private fun loadAllDocuments() {
        pdfArrayList = ArrayList()
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear list before adding data into it
                pdfArrayList.clear()
                for (ds in snapshot.children){
                    //get data
                    val model = ds.getValue(ModelDocument::class.java)
                    //add to list
                    pdfArrayList.add(model!!)
                }
                //setup adapter
                adapterDocumentUser = AdapterDocumentUser(context!!, pdfArrayList)
                //set adapter to recyclerview
                binding.documentsRv.adapter = adapterDocumentUser
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun loadMostViewedDocuments(orderBy: String) {
        pdfArrayList = ArrayList()
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        //load 10 most viewed documents
        ref.orderByChild(orderBy).limitToLast(10).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear list before adding data into it
                pdfArrayList.clear()
                for (ds in snapshot.children){
                    //get data
                    val model = ds.getValue(ModelDocument::class.java)
                    //add to list
                    pdfArrayList.add(model!!)
                }
                //setup adapter
                adapterDocumentUser = AdapterDocumentUser(context!!, pdfArrayList)
                //set adapter to recyclerview
                binding.documentsRv.adapter = adapterDocumentUser
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun loadCategorizedDocuments() {
        pdfArrayList = ArrayList()
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        //load 10 most viewed documents
        ref.orderByChild("categoryId").equalTo(categoryId).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear list before adding data into it
                pdfArrayList.clear()
                for (ds in snapshot.children){
                    //get data
                    val model = ds.getValue(ModelDocument::class.java)
                    //add to list
                    pdfArrayList.add(model!!)
                }
                //setup adapter
                adapterDocumentUser = AdapterDocumentUser(context!!, pdfArrayList)
                //set adapter to recyclerview
                binding.documentsRv.adapter = adapterDocumentUser
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

}