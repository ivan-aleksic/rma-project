package com.example.rma_project

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_project.databinding.RowDocumentUserBinding

class AdapterDocumentUser : RecyclerView.Adapter<AdapterDocumentUser.HolderDocumentUser>, Filterable {

    private var context: Context
    //arraylist to hold documents
    public var documentArrayList: ArrayList<ModelDocument> //public = to access filter class
    //arrayList to hold filtered documents
    public var filterList: ArrayList<ModelDocument>

    //viewBinding row_document_user.xml => RowDocumentUserBinding
    private lateinit var binding: RowDocumentUserBinding

    private var filter: FilterDocumentUser? = null

    constructor(context: Context, documentArrayList: ArrayList<ModelDocument>) {
        this.context = context
        this.documentArrayList = documentArrayList
        this.filterList = documentArrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderDocumentUser {
        //inflate/bind layout row_document_user.xml
        binding = RowDocumentUserBinding.inflate(LayoutInflater.from(context), parent, false)

        return HolderDocumentUser(binding.root)
    }

    override fun onBindViewHolder(holder: HolderDocumentUser, position: Int) {
        //get data
        val model = documentArrayList[position]
        val documentId = model.id
        val categoryId = model.categoryId
        val name = model.name
        val description = model.description
        val uid = model.uid
        val url = model.url
        val timestamp = model.timestamp

        //convert time
        val date = MyApplication.formatTimeStamp(timestamp)

        //set data
        holder.titleTv.text = name
        holder.descriptionTv.text = description
        holder.dateTv.text = date

        MyApplication.loadPdfFromUrlSinglePage(url, name, holder.pdfView, holder.progressBar, null)

        MyApplication.loadCompCategory(categoryId, holder.compCategoryTv)

        MyApplication.loadDocumentSize(url, name, holder.sizeTv)

        //handle click to open pdf details page
        holder.itemView.setOnClickListener {
            //pass document id in intent
            val intent = Intent(context, DocumentDetailActivity::class.java)
            intent.putExtra("pdfId", documentId)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return documentArrayList.size
    }

    override fun getFilter(): Filter {
        if (filter == null){
            filter = FilterDocumentUser(filterList, this)
        }
        return filter as FilterDocumentUser
    }

    /*ViewHolder class row_document_user.xml*/
    inner class HolderDocumentUser(itemView: View): RecyclerView.ViewHolder(itemView){
        //init UI components of row_document_user.xml
        var pdfView = binding.pdfView
        var progressBar = binding.progressBar
        var titleTv = binding.titleTv
        var descriptionTv = binding.descriptionTv
        var compCategoryTv = binding.compCategoryTv
        var sizeTv = binding.sizeTv
        var dateTv = binding.dateTv

    }

}