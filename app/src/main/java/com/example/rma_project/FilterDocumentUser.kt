package com.example.rma_project

import android.widget.Filter

class FilterDocumentUser: Filter {

    //array list - to search what we need
    var filterList: ArrayList<ModelDocument>

    //adapter where filter is implemented
    var adapterDocumentUser: AdapterDocumentUser

    //constructor
    constructor(filterList: ArrayList<ModelDocument>, adapterDocumentUser: AdapterDocumentUser) : super() {
        this.filterList = filterList
        this.adapterDocumentUser = adapterDocumentUser
    }

    override fun performFiltering(constraint: CharSequence): FilterResults {

        var constraint: CharSequence? = constraint
        val results = FilterResults()

        //value to be searched (not null and not empty)
        if (constraint != null && constraint.isNotEmpty()){
            //upper or lower case change to remove case sensitivity
            constraint = constraint.toString().uppercase()
            val filteredModels = ArrayList<ModelDocument>()
            for (i in filterList.indices){
                if (filterList[i].name.uppercase().contains(constraint)){
                    //validation of searched value (if matches with title, add to list)
                    filteredModels.add(filterList[i])
                }
            }
            //return filtered size and list
            results.count = filteredModels.size
            results.values = filteredModels

        }
        else {
            //null or empty - return original size and list
            results.count = filterList.size
            results.values = filterList
        }

        return results
    }

    override fun publishResults(constraint: CharSequence, results: FilterResults) {
        //filter changes apply
        adapterDocumentUser.documentArrayList = results.values as ArrayList<ModelDocument>

        //notify changes
        adapterDocumentUser.notifyDataSetChanged()
    }
}