package com.example.rma_project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.example.rma_project.databinding.ActivityDocumentListAdminBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DocumentListAdminActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding: ActivityDocumentListAdminBinding

    private companion object{
        const val TAG = "PDF_LIST_ADMIN_TAG"
    }

    //category id, name (title)
    private var categoryId = ""
    private var category = ""

    //arraylist to hold documents (pdf)
    private lateinit var pdfArrayList: ArrayList<ModelDocument>
    //adapter
    private lateinit var adapterDocumentAdmin: AdapterDocumentAdmin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentListAdminBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //get from intent (passed from adapter)
        val intent = intent
        categoryId = intent.getStringExtra("categoryId")!!
        category = intent.getStringExtra("category")!!

        //set document category
        binding.subTitleTv.text = category

        //load documents
        loadDocumentList()

        //search
        binding.searchEt.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence, p1: Int, p2: Int, p3: Int) {
                //filter data
                try {
                    adapterDocumentAdmin.filter!!.filter(s)
                }
                catch (e: Exception){
                    Log.d(TAG, "onTextChanged: ${e.message}")
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })

        //go back
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

    }

    private fun loadDocumentList() {
        //init arraylist
        pdfArrayList = ArrayList()

        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.orderByChild("categoryId").equalTo(categoryId)
            .addValueEventListener(object: ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    //clear list
                    pdfArrayList.clear()
                    for (ds in snapshot.children){
                        //get data
                        val model = ds.getValue(ModelDocument::class.java)
                        //add to list 
                        if (model != null) {
                            pdfArrayList.add(model)
                            Log.d(TAG, "onDataChange: ${model.name} ${model.categoryId}")
                        }
                    }
                    //setup adapter
                    adapterDocumentAdmin = AdapterDocumentAdmin(this@DocumentListAdminActivity, pdfArrayList)
                    binding.documentsRv.adapter = adapterDocumentAdmin
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }

}