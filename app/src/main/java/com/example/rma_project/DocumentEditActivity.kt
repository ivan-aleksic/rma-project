package com.example.rma_project

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.rma_project.databinding.ActivityDocumentEditBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DocumentEditActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding: ActivityDocumentEditBinding

    private companion object {
        private const val TAG = "PDF_EDIT_TAG"
    }

    //document id get from intent started from AdapterDocumentAdmin
    private var documentId = ""

    //progress dialog
    private lateinit var progressDialog: ProgressDialog

    //arraylist to hold competition category names
    private lateinit var compCategoryNameArrayList: ArrayList<String>

    //arraylist to hold competition category ids
    private lateinit var compCategoryIdArrayList: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentEditBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //get document id to edit the document info
        documentId = intent.getStringExtra("documentId")!!

        //setup progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Molimo pričekajte")
        progressDialog.setCanceledOnTouchOutside(false)

        loadCompCategories()
        loadDocumentInfo()

        //go back
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        //pick competition category
        binding.compCategoryTv.setOnClickListener {
            compCategoryDialog()
        }

        //begin update
        binding.submitBtn.setOnClickListener {
            validateData()
        }
    }

    private fun loadDocumentInfo() {
        Log.d(TAG, "loadDocumentInfo: Loading document info")

        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.child(documentId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    //get document info
                    selectedCompCategoryId = snapshot.child("categoryId").value.toString()
                    val description = snapshot.child("description").value.toString()
                    val name = snapshot.child("name").value.toString()

                    //set to views
                    binding.titleEt.setText(name)
                    binding.descriptionEt.setText(description)

                    //load document category info using categoryId
                    Log.d(TAG, "onDataChange: Loading document category info")
                    val refDocumentCategory =
                        FirebaseDatabase.getInstance().getReference("CompetitionCategories")
                    refDocumentCategory.child(selectedCompCategoryId)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                //get category
                                val category = snapshot.child("category").value
                                //set to textview
                                binding.compCategoryTv.text = category.toString()
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }
                        })
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }


    private var name = ""
    private var description = ""

    private fun validateData() {
        //get data
        name = binding.titleEt.text.toString().trim()
        description = binding.descriptionEt.text.toString().trim()

        //validate data
        if (name.isEmpty()) {
            Toast.makeText(this, "Unesite naziv dokumenta", Toast.LENGTH_SHORT).show()
        } else if (description.isEmpty()) {
            Toast.makeText(this, "Unesite kratki opis dokumenta", Toast.LENGTH_SHORT).show()
        } else if (selectedCompCategoryId.isEmpty()) {
            Toast.makeText(this, "Odaberite kategoriju natjecanja", Toast.LENGTH_SHORT).show()
        } else {
            updateDocument()
        }
    }

    private fun updateDocument() {
        Log.d(TAG, "updateDocument: Starting updating document info...")

        //show progress
        progressDialog.setMessage("Učitavanje informacija o dokumentu")
        progressDialog.show()

        //setup data to update to db
        val hashMap = HashMap<String, Any>()
        hashMap["name"] = "$name"
        hashMap["description"] = "$description"
        hashMap["categoryId"] = "$selectedCompCategoryId"

        //start updating
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.child(documentId)
            .updateChildren(hashMap)
            .addOnSuccessListener {
                progressDialog.dismiss()
                Log.d(TAG, "updateDocument: Updated successfully...")
                Toast.makeText(this, "Uspješno učitano...", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                Log.d(TAG, "updateDocument: Failed to update due to ${e.message}")
                progressDialog.dismiss()
                Toast.makeText(
                    this,
                    "Greška prilikom učitavanja zbog ${e.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    private var selectedCompCategoryId = ""
    private var selectedCompCategoryName = ""

    private fun compCategoryDialog() {
        /*Show dialog to pick the competition category of document
          that is already in saved categories*/

        //make string array from arraylist of string
        val compCategoriesArray = arrayOfNulls<String>(compCategoryNameArrayList.size)
        for (i in compCategoryNameArrayList.indices) {
            compCategoriesArray[i] = compCategoryNameArrayList[i]
        }

        //alert dialog
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Odaberite kategoriju natjecanja")
            .setItems(compCategoriesArray) { dialog, position ->
                //save clicked competition category id and name
                selectedCompCategoryId = compCategoryIdArrayList[position]
                selectedCompCategoryName = compCategoryNameArrayList[position]

                //set to textview
                binding.compCategoryTv.text = selectedCompCategoryName
            }
            .show() //show dialog
    }

    private fun loadCompCategories() {
        Log.d(TAG, "loadCompCategories: loading categories...")

        compCategoryNameArrayList = ArrayList()
        compCategoryIdArrayList = ArrayList()

        val ref = FirebaseDatabase.getInstance().getReference("CompetitionCategories")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                compCategoryIdArrayList.clear()
                compCategoryNameArrayList.clear()

                for (ds in snapshot.children) {
                    val id = "${ds.child("id").value}"
                    val category = "${ds.child("category").value}"

                    compCategoryIdArrayList.add(id)
                    compCategoryNameArrayList.add(category)

                    Log.d(TAG, "onDataChange: CompetitionCategory ID $id")
                    Log.d(TAG, "onDataChange: CompetitionCategory category $category")
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

    }
}
