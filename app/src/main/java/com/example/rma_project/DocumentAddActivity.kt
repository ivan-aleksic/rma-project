package com.example.rma_project

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.rma_project.databinding.ActivityDocumentAddBinding
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage


class DocumentAddActivity : AppCompatActivity() {

    //setup view binding
    private lateinit var binding: ActivityDocumentAddBinding

    //firebase auth
    private lateinit var firebaseAuth: FirebaseAuth

    //progress dialog (while uploading document)
    private lateinit var progressDialog: ProgressDialog

    //arrayList to hold pdf categories
    private lateinit var compCategoryArrayList: ArrayList<ModelCompCategory>

    //uri of picked document
    private var pdfUri: Uri? = null

    //TAG
    private val TAG = "PDF_ADD_TAG"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //init firebase auth
        firebaseAuth = FirebaseAuth.getInstance()
        loadDocumentCategories()

        //setup progress dialog
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Molimo pričekajte")
        progressDialog.setCanceledOnTouchOutside(false)

        //go back
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        //show competition category pick dialog
        binding.compCategoryTv.setOnClickListener {
            compCategoryPickDialog()
        }

        //pick document (pdf) intent
        binding.attachDocumentBtn.setOnClickListener {
            documentPickIntent()
        }

        //start uploading document (pdf)
        binding.submitBtn.setOnClickListener {
            validateData()
        }
    }

    private var name = ""
    private var description = ""
    private var compCategory = ""

    private fun validateData() {
        //validate data
        Log.d(TAG, "validateData: validating data")

        //get data
        name = binding.titleEt.text.toString().trim()
        description = binding.descriptionEt.text.toString().trim()
        compCategory = binding.compCategoryTv.text.toString().trim()

        //validate data
        if (name.isEmpty()){
            Toast.makeText(this, "Unesite naziv dokumenta...", Toast.LENGTH_SHORT).show()
        }
        else if (description.isEmpty()){
            Toast.makeText(this, "Unesite kratki opis dokumenta...", Toast.LENGTH_SHORT).show()
        }
        else if (compCategory.isEmpty()){
            Toast.makeText(this, "Odaberite kategoriju natjecanja...", Toast.LENGTH_SHORT).show()
        }
        else if (pdfUri == null){
            Toast.makeText(this, "Odaberite dokument...", Toast.LENGTH_SHORT).show()
        }
        else{
            //begin upload
            uploadDocumentToStorage()
        }
    }

    private fun uploadDocumentToStorage() {
        //upload document to firebase storage
        Log.d(TAG, "uploadDocumentToStorage: uploading to storage...")

        //show progress dialog
        progressDialog.setMessage("Ucitavanje dokumenta...")
        progressDialog.show()

        //timestamp
        val timestamp = System.currentTimeMillis()

        //path of document in firebase storage
        val filePathAndName = "Documents/$timestamp"
        //storage reference
        val storageReference = FirebaseStorage.getInstance().getReference(filePathAndName)
        storageReference.putFile(pdfUri!!)
            .addOnSuccessListener {taskSnapshot->
                Log.d(TAG, "uploadDocumentToStorage: Document uploaded now getting url...")

                //get url of document
                val uriTask: Task<Uri> = taskSnapshot.storage.downloadUrl
                while(!uriTask.isSuccessful);
                val uploadedPdfUrl = "${uriTask.result}"

                uploadDocumentInfoToDb(uploadedPdfUrl, timestamp)
            }
            .addOnFailureListener{e->
                Log.d(TAG, "uploadDocumentToStorage: failed to upload due to ${e.message}")
                progressDialog.dismiss()
                Toast.makeText(this, "Greška prilikom učitavanja dokumenta zbog ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun uploadDocumentInfoToDb(uploadedPdfUrl: String, timestamp: Long) {
        //upload document to firebase database
        Log.d(TAG, "uploadedDocumentInfoToDb: uploading to database")
        progressDialog.setMessage("Ucitavanje informacija o dokumentu...")

        //uid of current user
        val uid = firebaseAuth.uid

        //setup data to upload
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["uid"] = "$uid"
        hashMap["id"] = "$timestamp"
        hashMap["name"] = "$name"
        hashMap["description"] = "$description"
        hashMap["categoryId"] = "$selectedCompCategoryId"
        hashMap["url"] = "$uploadedPdfUrl"
        hashMap["timestamp"] = timestamp
        hashMap["viewsCount"] = 0
        hashMap["downloadsCount"] = 0

        //database reference
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.child("$timestamp")
            .setValue(hashMap)
            .addOnSuccessListener {
                Log.d(TAG, "uploadDocumentInfoToDb: uploaded to database")
                progressDialog.dismiss()
                Toast.makeText(this, "Učitavanje...", Toast.LENGTH_SHORT).show()
                pdfUri = null
            }
            .addOnFailureListener{ e->
                Log.d(TAG, "uploadDocumentInfoToDb: failed to upload due to ${e.message}")
                progressDialog.dismiss()
                Toast.makeText(this, "Greška prilikom učitavanja dokumenta zbog ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun loadDocumentCategories() {
        Log.d(TAG, "loadDocumentCategories: Loading document categories")
        //init arrayList
        compCategoryArrayList = ArrayList()

        //database reference to load categories
        val ref = FirebaseDatabase.getInstance().getReference("CompetitionCategories")
        ref.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear list before adding data
                compCategoryArrayList.clear()
                for (ds in snapshot.children) {
                    //get data
                    val model = ds.getValue(ModelCompCategory::class.java)
                    //add to arrayList
                    compCategoryArrayList.add(model!!)
                    Log.d(TAG, "onDataChange: ${model.category}")
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private var selectedCompCategoryId = ""
    private var selectedCompCategoryName = ""

    private fun compCategoryPickDialog(){
        Log.d(TAG, "compCategoryPickDialog: Showing document competition category dialog")

        //get string array of competition categories from arrayList
        val compCategoriesArray = arrayOfNulls<String>(compCategoryArrayList.size)
        for (i in compCategoryArrayList.indices){
            compCategoriesArray[i] = compCategoryArrayList[i].category
        }

        //alert dialog
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Odaberite kategoriju natjecanja")
            .setItems(compCategoriesArray){dialog, which ->
                //item click
                //get clicked item
                selectedCompCategoryName = compCategoryArrayList[which].category
                selectedCompCategoryId = compCategoryArrayList[which].id
                //set category to textview
                binding.compCategoryTv.text = selectedCompCategoryName

                Log.d(TAG, "compCategoryPickDialog: Selected Competition Category ID: $selectedCompCategoryId")
                Log.d(TAG, "compCategoryPickDialog: Selected Competition Category Name: $selectedCompCategoryName")
            }
            .show()
    }

    private fun documentPickIntent(){
        Log.d(TAG, "documentPickIntent: starting document pick intent")

        val intent = Intent()
        intent.type = "application/pdf"
        intent.action = Intent.ACTION_GET_CONTENT
        documentActivityResultLauncher.launch(intent)
    }

    val documentActivityResultLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult(),
        ActivityResultCallback<ActivityResult>{result ->
            if (result.resultCode == RESULT_OK){
                Log.d(TAG, "Document (PDF) Picked: ")
                pdfUri = result.data!!.data
            }
            else{
                Log.d(TAG, "Document (PDF) cancelled: ")
                Toast.makeText(this, "Otkazano", Toast.LENGTH_SHORT).show()
            }
        }
    )

}