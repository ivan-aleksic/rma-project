package com.example.rma_project

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.example.rma_project.databinding.ActivityDashboardAdminBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.lang.Exception

class DashboardAdminActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding: ActivityDashboardAdminBinding

    //firebase auth
    private lateinit var firebaseAuth: FirebaseAuth

    //arrayList to hold competition categories
    private lateinit var compCategoryArrayList: ArrayList<ModelCompCategory>
    //adapter
    private lateinit var adapterCompCategory: AdapterCompCategory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardAdminBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //init firebase Auth
        firebaseAuth = FirebaseAuth.getInstance()
        checkUser()
        loadCompCategories()

        //search
        binding.searchEt.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //when user type anything
                try {
                    adapterCompCategory.filter.filter(s)
                }
                catch (e: Exception){

                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })

        //logout
        binding.logoutBtn.setOnClickListener {
            firebaseAuth.signOut()
            checkUser()
        }

        //add competition category
        binding.addCompCategoryBtn.setOnClickListener {
            startActivity(Intent(this, CompCategoryAddActivity::class.java))
        }

        //add document page
        binding.addDocumentFab.setOnClickListener {
            startActivity(Intent(this, DocumentAddActivity::class.java))
        }
    }

    private fun loadCompCategories() {
        //init arrayList
        compCategoryArrayList = ArrayList()

        //get all competition categories from firebase database
        val ref = FirebaseDatabase.getInstance().getReference("CompetitionCategories")
        ref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //clear list before adding data into it
                compCategoryArrayList.clear()
                for(ds in snapshot.children){
                    //get data as model
                    val model = ds.getValue(ModelCompCategory::class.java)

                    //add to arrayList
                    compCategoryArrayList.add(model!!)
                }
                //setup adapter
                adapterCompCategory = AdapterCompCategory(this@DashboardAdminActivity, compCategoryArrayList)
                //set adapter to recyclerview
                binding.compCategoriesRv.adapter = adapterCompCategory
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun checkUser() {
        //get current user
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null){
            //not logged in, go to main screen
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        else{
            //get and show user info
            val email = firebaseUser.email
            binding.subTitleTv.text = email
        }
    }
}