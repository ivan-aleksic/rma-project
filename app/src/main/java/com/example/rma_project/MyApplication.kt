package com.example.rma_project

import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.github.barteksc.pdfviewer.PDFView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import java.util.*
import kotlin.collections.HashMap

class MyApplication:Application() {

    override fun onCreate() {
        super.onCreate()
    }

    companion object{

        //created a static method to convert timestamp to proper date format
        fun formatTimeStamp(timestamp: Long) : String{
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = timestamp
            //format dd/MM/yyyy
            return DateFormat.format("dd/MM/yyyy", cal).toString()
        }

        //function to get document size
        fun loadDocumentSize(pdfUrl: String, pdfTitle: String, sizeTv: TextView){
            val TAG = "PDF_SIZE_TAG"
            
            //using url to get file and its metadata from firebase storage
            val ref = FirebaseStorage.getInstance().getReferenceFromUrl(pdfUrl)
            ref.metadata
                .addOnSuccessListener {storageMetaData->
                    Log.d(TAG, "loadDocumentSize: got metadata")
                    val bytes = storageMetaData.sizeBytes.toDouble()
                    Log.d(TAG, "loadDocumentSize: Size Bytes $bytes")

                    //convert bytes to KB/MB
                    val kb = bytes/1024
                    val mb = kb/1024
                    if(mb>1) {
                        sizeTv.text = "${String.format("%.2f", mb)} MB"
                    }
                    else if (kb>=1){
                        sizeTv.text = "${String.format("%.2f", kb)} KB"
                    }
                    else {
                        sizeTv.text = "${String.format("%.2f", bytes)} bytes"
                    }
                }
                .addOnFailureListener { e->
                    //failed to get metadata
                    Log.d(TAG, "loadDocumentSize: Failed to get metadata due to ${e.message}")
                }
        }

        fun loadPdfFromUrlSinglePage(
            pdfUrl: String,
            pdfTitle: String,
            pdfView: PDFView,
            progressBar: ProgressBar,
            pagesTv: TextView?
        ){
            val TAG = "PDF_THUMBNAIL_TAG"

            //using url to get file and its metadata from firebase storage
            val ref = FirebaseStorage.getInstance().getReferenceFromUrl(pdfUrl)
            ref.getBytes(Constants.MAX_BYTES_PDF)
                .addOnSuccessListener {bytes ->
                    Log.d(TAG, "loadDocumentSize: Size Bytes $bytes")

                    //SET TO PDFVIEW
                    pdfView.fromBytes(bytes)
                        .pages(0) //show first page only
                        .spacing(0)
                        .swipeHorizontal(false)
                        .enableSwipe(false)
                        .onError { t->
                            progressBar.visibility = View.INVISIBLE
                            Log.d(TAG, "loadPdfFromUrlSinglePage: ${t.message}")
                        }
                        .onPageError { page, t ->
                            progressBar.visibility = View.INVISIBLE
                            Log.d(TAG, "loadPdfFromUrlSinglePage: ${t.message}")
                        }
                        .onLoad { nbPages ->
                            Log.d(TAG, "loadPdfFromUrlSinglePage: Pages: $nbPages")
                            //pdf loaded, set page count, pdf thumbnail
                            progressBar.visibility = View.INVISIBLE

                            //if pagesTv param is not null -> set page numbers
                            if (pagesTv != null){
                                pagesTv.text = "$nbPages"
                            }
                        }
                        .load()
                }
                .addOnFailureListener { e->
                    //failed to get metadata
                    Log.d(TAG, "loadDocumentSize: Failed to get metadata due to ${e.message}")
                }
        }

        fun loadCompCategory(categoryId: String, compCategoryTv: TextView) {
            //load competition category using category id from firebase
            val ref = FirebaseDatabase.getInstance().getReference("CompetitionCategories")
            ref.child(categoryId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        //get category
                        val category = "${snapshot.child("category").value}"

                        //set category
                        compCategoryTv.text = category
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }

        fun deleteDocument(context: Context, documentId: String, documentUrl: String, documentName: String){
            val TAG = "DELETE_BOOK_TAG"

            Log.d(TAG, "deleteDocument: deleting...")

            //progress dialog
            val progressDialog = ProgressDialog(context)
            progressDialog.setTitle("Molimo pričekajte")
            progressDialog.setMessage("Brisanje $documentName...")
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()

            Log.d(TAG, "deleteDocument: Deleting from storage...")
            val storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(documentUrl)
            storageReference.delete()
                .addOnSuccessListener {
                    Log.d(TAG, "deleteDocument: Deleted from storage")
                    Log.d(TAG, "deleteDocument: Deleting from database now...")

                    val ref = FirebaseDatabase.getInstance().getReference("Documents")
                    ref.child(documentId)
                        .removeValue()
                        .addOnSuccessListener { e->
                            progressDialog.dismiss()
                            Toast.makeText(context, "Uspješno brisanje...", Toast.LENGTH_SHORT).show()
                            Log.d(TAG, "deleteDocument: Deleted from db too...")
                        }
                        .addOnFailureListener {e->
                            progressDialog.dismiss()
                            Log.d(TAG, "deleteDocument: Failed to delete from database due to ${e.message}")
                            Toast.makeText(context, "Greška prilikom brisanja zbog ${e.message}", Toast.LENGTH_SHORT).show()
                        }
                }
                .addOnFailureListener { e->
                    progressDialog.dismiss()
                    Log.d(TAG, "deleteDocument: Failed to delete from storage due to ${e.message}")
                    Toast.makeText(context, "Greška prilikom brisanja zbog ${e.message}", Toast.LENGTH_SHORT).show()
                }
        }

        fun incrementDocumentViewCount(documentId: String){
            //1 - get current views count
            val ref = FirebaseDatabase.getInstance().getReference("Documents")
            ref.child(documentId)
                .addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        //get views count
                        var viewsCount = "${snapshot.child("viewsCount").value}"

                        if(viewsCount=="" || viewsCount=="null"){
                            viewsCount = "0";
                        }

                        //2 - Increment views count
                        val newViewsCount = viewsCount.toLong() + 1

                        //setup data to update in db
                        val hashMap = HashMap<String, Any>()
                        hashMap["viewsCount"] = newViewsCount

                        //set to db
                        val dbRef = FirebaseDatabase.getInstance().getReference("Documents")
                        dbRef.child(documentId)
                            .updateChildren(hashMap)
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }

    }
}