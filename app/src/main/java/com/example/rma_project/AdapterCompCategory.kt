package com.example.rma_project

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_project.databinding.RowCompCategoryBinding
import com.google.firebase.database.FirebaseDatabase

class AdapterCompCategory :RecyclerView.Adapter<AdapterCompCategory.HolderCompCategory>, Filterable{

    private val context: Context
    public var compCategoryArrayList: ArrayList<ModelCompCategory>
    private var filterList: ArrayList<ModelCompCategory>

    private var filter: FilterCompCategory? = null

    private lateinit var binding: RowCompCategoryBinding
    //constructor
    constructor(context: Context, compCategoryArrayList: ArrayList<ModelCompCategory>) {
        this.context = context
        this.compCategoryArrayList = compCategoryArrayList
        this.filterList = compCategoryArrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderCompCategory {
        //bind row_compCategory.xml
        binding = RowCompCategoryBinding.inflate(LayoutInflater.from(context), parent, false)

        return HolderCompCategory(binding.root)
    }

    override fun onBindViewHolder(holder: HolderCompCategory, position: Int) {
        //get data
        val model = compCategoryArrayList[position]
        val id = model.id
        val category = model.category
        val uid = model.uid
        val timestamp = model.timestamp

        //set data
        holder.compCategoryTv.text = category

        //delete category
        holder.deleteBtn.setOnClickListener {
            //confirm before delete
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Obriši natjecanje")
                .setMessage("Jeste li sigurni kako želite obrisati ovu kategoriju natjecanja?")
                .setPositiveButton("Potvrdi") {a, d->
                    Toast.makeText(context, "Brisanje...", Toast.LENGTH_SHORT).show()
                    deleteCompCategory(model, holder)
                }
                .setNegativeButton("Zatvori"){a, d->
                    a.dismiss()
                }
                .show()
        }

        //start document list admin activity (pass pdf id, title)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DocumentListAdminActivity::class.java)
            intent.putExtra("categoryId", id)
            intent.putExtra("category", category)
            context.startActivity(intent)
        }
    }

    private fun deleteCompCategory(model: ModelCompCategory, holder: HolderCompCategory) {
        //get id of competition category
        val id = model.id

        val ref = FirebaseDatabase.getInstance().getReference("CompetitionCategories")
        ref.child(id)
            .removeValue()
            .addOnSuccessListener {
                Toast.makeText(context, "Uklonjeno...", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e->
                Toast.makeText(context, "Nije moguće ukloniti zbog ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    override fun getItemCount(): Int {
        return compCategoryArrayList.size //number of items in list
    }

    //ViewHolder class
    inner class HolderCompCategory(itemView: View): RecyclerView.ViewHolder(itemView){
        var compCategoryTv:TextView = binding.compCategoryTv
        var deleteBtn:ImageButton = binding.deleteBtn
    }

    override fun getFilter(): Filter {
        if (filter == null){
            filter = FilterCompCategory(filterList, this)
        }
        return filter as FilterCompCategory
    }


}