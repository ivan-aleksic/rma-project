package com.example.rma_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.rma_project.databinding.ActivityDocumentDetailBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DocumentDetailActivity : AppCompatActivity() {

    //view binding
    private lateinit var binding:ActivityDocumentDetailBinding

    //document id
    private var documentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //get document id from intent
        documentId = intent.getStringExtra("pdfId")!!

        //increment document view count
        MyApplication.incrementDocumentViewCount(documentId)

        loadDocumentDetails()

        //handle back button click
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        //handle click for open document view activity
        binding.readDocumentButton.setOnClickListener {
            val intent = Intent(this, DocumentViewActivity::class.java)
            intent.putExtra("documentId", documentId);
            startActivity(intent)
        }
    }

    private fun loadDocumentDetails() {
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.child(documentId)
            .addListenerForSingleValueEvent(object: ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val categoryId = "${snapshot.child("categoryId").value}"
                    val description = "${snapshot.child("description").value}"
                    //val downloadsCount = "${snapshot.child("downloadsCount").value}"
                    val timestamp = "${snapshot.child("timestamp").value}"
                    val name = "${snapshot.child("name").value}"
                    val uid = "${snapshot.child("uid").value}"
                    val url = "${snapshot.child("url").value}"
                    val viewsCount = "${snapshot.child("viewsCount").value}"

                    //format date
                    val date = MyApplication.formatTimeStamp(timestamp.toLong())

                    //load document category
                    MyApplication.loadCompCategory(categoryId, binding.categoryTv)

                    //load document thumbnail, pages count
                    MyApplication.loadPdfFromUrlSinglePage("$url", "$name", binding.pdfView, binding.progressBar, binding.pagesTv)

                    //load document size
                    MyApplication.loadDocumentSize("$url", "$name", binding.sizeTv)

                    //set data
                    binding.titleTv.text = name
                    binding.descriptionTv.text = description
                    binding.viewsTv.text = viewsCount
                    binding.dateTv.text = date

                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }
}