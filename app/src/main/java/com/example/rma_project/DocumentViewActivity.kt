package com.example.rma_project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.rma_project.databinding.ActivityDocumentViewBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage

class DocumentViewActivity : AppCompatActivity() {

    //View binding
    private lateinit var binding: ActivityDocumentViewBinding

    //TAG
    private companion object{
        const val TAG = "PDF_VIEW_TAG"
    }

    //document id
    var documentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //get document id
        documentId = intent.getStringExtra("documentId")!!
        loadDocumentDetails()

        //handle click, go back
        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

    }

    private fun loadDocumentDetails() {
        Log.d(TAG, "loadDocumentDetails: Get Document URL from db")
        //Get Document URL using Document Id
        val ref = FirebaseDatabase.getInstance().getReference("Documents")
        ref.child(documentId)
            .addListenerForSingleValueEvent(object: ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    //get document url
                    val documentUrl = snapshot.child("url").value
                    Log.d(TAG, "onDataChange: PDF_URL: $documentUrl")

                    //load document using url
                    loadDocumentFromUrl("$documentUrl")
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }

    private fun loadDocumentFromUrl(documentUrl: String) {
        Log.d(TAG, "loadDocumentFromUrl: Get Pdf from firebase storage using URL")

        val reference = FirebaseStorage.getInstance().getReferenceFromUrl(documentUrl)
        reference.getBytes(Constants.MAX_BYTES_PDF)
            .addOnSuccessListener {bytes->
                Log.d(TAG, "loadDocumentFromUrl: pdf got from url")

                //load document
                binding.pdfView.fromBytes(bytes)
                    .swipeHorizontal(false) //set false to scroll vertical, set true to scroll horizontal
                    .onPageChange{page, pageCount->
                        //set current and total pages in toolbar subtitle
                        val currentPage = page+1
                        binding.toolbarSubtitleTv.text = "$currentPage/$pageCount"
                        Log.d(TAG, "loadDocumentFromUrl: $currentPage/$pageCount")
                    }
                    .onError { t->
                        Log.d(TAG, "loadDocumentFromUrl: ${t.message}")
                    }
                    .onPageError { page, t ->
                        Log.d(TAG, "loadDocumentFromUrl: ${t.message}")
                    }
                    .load()
                binding.progressBar.visibility = View.GONE
            }
            .addOnFailureListener { e->
                Log.d(TAG, "loadDocumentFromUrl: Failed to get pdf due to ${e.message}")
                binding.progressBar.visibility = View.GONE
            }
    }
}