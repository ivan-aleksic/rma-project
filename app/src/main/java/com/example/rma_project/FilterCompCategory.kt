package com.example.rma_project

import android.widget.Filter

class FilterCompCategory: Filter{

    //arrayList in which we want to search
    private var filterList: ArrayList<ModelCompCategory>

    //adapter in which filter need to be implemented
    private var adapterCompCategory: AdapterCompCategory

    //constructor
    constructor(
        filterList: ArrayList<ModelCompCategory>,
        adapterCompCategory: AdapterCompCategory
    ) : super() {
        this.filterList = filterList
        this.adapterCompCategory = adapterCompCategory
    }

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        var constraint = constraint
        val results = FilterResults()

        //value should not be null or empty
        if (constraint != null && constraint.isNotEmpty()){
            //searched value is nor null not empty

            //change to upper case, or lower case to avoid case sensitivity
            constraint = constraint.toString().uppercase()
            val filteredModels:ArrayList<ModelCompCategory> = ArrayList()
            for (i in 0 until filterList.size){
                //validate
                if (filterList[i].category.uppercase().contains(constraint)){
                    //add to filtered list
                    filteredModels.add(filterList[i])
                }
            }
            results.count = filteredModels.size
            results.values = filteredModels

        }
        else{
            //search value is either null or empty
            results.count = filterList.size
            results.values = filterList
        }

        return results
    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults) {
        //apply filter changes
        adapterCompCategory.compCategoryArrayList = results.values as ArrayList<ModelCompCategory>

        //notify changes
        adapterCompCategory.notifyDataSetChanged()
    }
}