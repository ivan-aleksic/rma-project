package com.example.rma_project

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_project.databinding.RowDocumentAdminBinding

class AdapterDocumentAdmin :RecyclerView.Adapter<AdapterDocumentAdmin.HolderDocumentAdmin>, Filterable{

    //context
    private var context: Context
    //arraylist to hold documents
    public var pdfArrayList: ArrayList<ModelDocument>
    private val filterList:ArrayList<ModelDocument>

    //viewBinding
    private lateinit var binding: RowDocumentAdminBinding

    //filter object
    private var filter: FilterDocumentAdmin? = null

    //constructor
    constructor(context: Context, pdfArrayList: ArrayList<ModelDocument>) : super() {
        this.context = context
        this.pdfArrayList = pdfArrayList
        this.filterList = pdfArrayList
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderDocumentAdmin {
        //bind/inflate layout row_pdf_admin.xml
        binding = RowDocumentAdminBinding.inflate(LayoutInflater.from(context), parent, false)

        return HolderDocumentAdmin(binding.root)
    }

    override fun onBindViewHolder(holder: HolderDocumentAdmin, position: Int) {
        //get data
        val model = pdfArrayList[position]
        val pdfId = model.id
        val categoryId = model.categoryId
        val name = model.name
        val description = model.description
        val pdfUrl = model.url
        val timestamp = model.timestamp
        //convert timestamp to dd/MM/yyyy format
        val formattedDate = MyApplication.formatTimeStamp(timestamp)

        //set data
        holder.titleTv.text = name
        holder.descriptionTv.text = description
        holder.dateTv.text = formattedDate

        //load further details (category, pdf from url, pdf size

        //load category
        MyApplication.loadCompCategory(categoryId, holder.compCategoryTv)

        //pass null for page number (don't need it) || load pdf thumbnail
        MyApplication.loadPdfFromUrlSinglePage(pdfUrl, name, holder.pdfView, holder.progressBar, null)

        //load pdf size
        MyApplication.loadDocumentSize(pdfUrl, name, holder.sizeTv)

        //show dialog with options (edit book or delete book)
        holder.moreBtn.setOnClickListener {
            moreOptionsDialog(model, holder)
        }

        //handle item click
        holder.itemView.setOnClickListener {
            //intent with document id
            val intent = Intent(context, DocumentDetailActivity::class.java)
            intent.putExtra("pdfId", pdfId)
            context.startActivity(intent)
        }
    }

    private fun moreOptionsDialog(model: ModelDocument, holder: AdapterDocumentAdmin.HolderDocumentAdmin) {
        //get id, url, name of document
        val documentId = model.id
        val documentUrl = model.url
        val documentName = model.name

        //options to show in dialog
        val options = arrayOf("Uredi dokument", "Obriši dokument")

        //alert dialog
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Odaberite opciju")
            .setItems(options){dialog, position->
                if(position==0) {
                    //Edit is clicked
                    val intent = Intent(context, DocumentEditActivity::class.java)
                    intent.putExtra("documentId", documentId)
                    context.startActivity(intent)
                }
                else if(position == 1){
                    //Delete is clicked
                    MyApplication.deleteDocument(context, documentId, documentUrl, documentName)
                }
            }
            .show()
    }

    override fun getItemCount(): Int {
        return pdfArrayList.size //items count
    }

    override fun getFilter(): Filter {
        if (filter == null){
            filter = FilterDocumentAdmin(filterList, this)
        }
        return filter as FilterDocumentAdmin
    }

    /*View Holder class for row_document_admin.xml*/
    inner class HolderDocumentAdmin(itemView: View): RecyclerView.ViewHolder(itemView){
        //UI Views of row_document_admin.xml
        val pdfView = binding.pdfView
        val progressBar = binding.progressBar
        val titleTv = binding.titleTv
        val descriptionTv = binding.descriptionTv
        val compCategoryTv = binding.compCategoryTv
        val sizeTv = binding.sizeTv
        val dateTv = binding.dateTv
        val moreBtn = binding.moreBtn
    }
}