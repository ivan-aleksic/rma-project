package com.example.rma_project

import android.widget.Filter

//used to search documents (pdf) from list of documents in recyclerview
class FilterDocumentAdmin : Filter{
    //arrayList in which we want to search
    var filterList: ArrayList<ModelDocument>

    //adapter in which filter need to be implemented
    var adapterPdfAdmin: AdapterDocumentAdmin

    //constructor
    constructor(filterList: ArrayList<ModelDocument>, adapterPdfAdmin: AdapterDocumentAdmin) {
        this.filterList = filterList
        this.adapterPdfAdmin = adapterPdfAdmin
    }

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        var constraint:CharSequence? = constraint//value to search
        val results = FilterResults()
        //value to be searched should not be null and not empty
        if (constraint != null && constraint.isNotEmpty()){
            //change to upper case, or lowercase to avoid sensitivity
            constraint = constraint.toString().lowercase()
            var filteredModels = ArrayList<ModelDocument>()
            for (i in filterList.indices){
                //validate if match
                if (filterList[i].name.lowercase().contains(constraint)){
                    //searched value is similar to value in list, add to filtered list
                    filteredModels.add(filterList[i])
                }
            }
            results.count = filteredModels.size
            results.values = filteredModels
        }
        else{
            //searched value is either null or empty
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(constraint: CharSequence, results: FilterResults) {
        //apply filter changes
        adapterPdfAdmin.pdfArrayList = results.values as ArrayList<ModelDocument>

        //notify changes
        adapterPdfAdmin.notifyDataSetChanged()
    }
}