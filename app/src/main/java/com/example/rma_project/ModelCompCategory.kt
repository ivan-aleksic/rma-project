package com.example.rma_project

class ModelCompCategory {

    //variables (matches with firebase)
    var id:String = ""
    var category:String = ""
    var timestamp:Long = 0
    var uid:String = ""

    //empty constructor (required by firebase)
    constructor()

    //parametrize constructor
    constructor(id: String, category: String, timestamp: Long, uid: String) {
        this.id = id
        this.category = category
        this.timestamp = timestamp
        this.uid = uid
    }
}